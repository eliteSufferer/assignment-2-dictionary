%macro colon 2
%2: 
    %ifdef last
        dq last
    %else
        dq 0
    %endif
db %1, 0
%define last %2
%endmacro
