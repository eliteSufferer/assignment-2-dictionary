%include "lib.inc"

section .text
global find_word

find_word:
    push r12
    push r13
    mov r12, rdi
    mov r13, rsi

    .loop:
        test r13, r13
        je .exit_fail
 
        mov rdi, r12
        mov rsi, r13

        add rsi, 8

        call string_equals
        cmp rax, 1
        je .exit_success
        mov r13, [r13]

        jmp .loop

    .exit_fail:
        xor rax, rax
        jmp .finish

    .exit_success:
        mov rax, r13

    .finish:
         pop r13
         pop r12
         ret
