import subprocess
long_str = "a"*512
input_data = ["third_word", "second_word", "first_word", "ahahah",
                      long_str]

success_codes = ["Another boring sentence", "I forgot that there is Python files support in CLion so I "
                                                    "installed PyCharm, damn", "I can show creative, but I will not", "", ""]

error_codes = ["", "", "", "No such word in the dict", "An error occurred while reading the string"]

path = "./my_program"

for i, test_input in enumerate(input_data):
    process = subprocess.run(path, input=test_input, text=True, capture_output=True)

    stdout = process.stdout.strip()
    stderr = process.stderr.strip()
    if stdout != success_codes[i]:
        print(f"Test{i+1} failed: got {stdout}, expected {success_codes[i]}")
    elif stderr != error_codes[i]:
        print(f"Test{i+1} failed: got {stderr}, expected {error_codes[i]}")
    else:
        print(f"Test{i+1} passed!")

    # print(str_out)
    # print(str_err)

    print("--------------------------")